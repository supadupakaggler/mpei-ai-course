import numpy as np

from tqdm import trange

from decisiontree import DecisionTreeClassifier


class RandomForestClassifier:
    def __init__(self, n_estimators=10):
        # TODO: doc it
        self.L = n_estimators

    def fit(self, X, y):
        # TODO: doc it
        n_samples, n_features = X.shape

        self.D = n_features
        self.dl = int(np.sqrt(self.D))

        self.trees = [DecisionTreeClassifier() for _ in range(self.L)]

        self.features_sets = [self._bootstrap(X, self.dl) for _ in range(self.L)]
        [t.fit(X[:, features], y) for t, features in zip(self.trees, self.features_sets)]

    def predict(self, X):
        trees_y = [t.predict(X[:, features]) for t, features in zip(self.trees, self.features_sets)]

        def vote(predictions):
            cls, counts = np.unique(predictions, return_counts=True)
            return cls[np.argmax(counts)]

        y = np.apply_along_axis(vote, 0, trees_y)
        return y

    def _bootstrap(self, X, k_features):
        # TODO: doc it
        features = np.arange(X.shape[1])
        chosen_features = np.random.choice(features, k_features, replace=True)
        return chosen_features
