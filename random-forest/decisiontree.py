import numpy as np


class DecisionTreeClassifier:
    def __init__(self, max_depth=None):
        self.max_depth = max_depth

    def fit(self, X, y):
        self.tree = self._buid_tree(X, y, self.max_depth)

    def __str__(self):
        return str(self.tree)

    def predict(self, X):
        return np.apply_along_axis(self._predict_one, 1, X)

    def _predict_one(self, Xi):
        node = self.tree
        while not node.isLeaf:
            node = node.childrens[Xi[node.label] > node.threshold]
        return node.label

    def _buid_tree(self, X, y, depth):
        if depth is not None:
            depth -= 1
        classes, counts = np.unique(y, return_counts=True)
        if len(classes) == 1:
            node = Node(classes[0], leaf=True)
        elif depth == 0:
            most_common_class = classes[np.argmax(counts)]
            node = Node(most_common_class, leaf=True)
        else:
            feature_idx, feature_th, split = self._get_best_split(X, y)
            node = Node(feature_idx, threshold=feature_th)
            node.childrens = [
                self._buid_tree(X[split == s], y[split == s], depth) for s in np.unique(split)
            ]
        return node

    def _get_best_split(self, X, y):
        entropies = np.apply_along_axis(self._entropy_for_feature, 0, X, y)
        feature_idx, _ = np.argmax(entropies, axis=1)
        feature_th = entropies[1, feature_idx]
        split = (X[:, feature_idx] > feature_th).astype(int)
        return feature_idx, feature_th, split

    def _entropy_for_feature(self, Xi, y):
        splits = (Xi > Xi[:, np.newaxis]).astype(int)
        entropies = np.apply_along_axis(self._gain, 1, splits, y)

        idx = np.argmax(entropies)
        max_entropy = entropies[idx]
        best_threshold = Xi[idx]
        return max_entropy, best_threshold

    def _gain(self, split, y):
        info = self._entropy(y)
        s, c = np.unique(split, return_counts=True)
        info_x = np.sum(c/len(y) * [self._entropy(y[split == si]) for si in s])
        return info - info_x

    def _entropy(self, T):
        S = len(T)
        _, num_classes = np.unique(T, return_counts=True)
        return -np.sum(num_classes/S * np.log2(num_classes/S))


class Node:
    def __init__(self, label, threshold=None, leaf=False):
        self.label = label
        self.threshold = threshold
        self.isLeaf = leaf
        self.childrens = []

    def __str__(self, level=0):
        ret = "\t"*level+repr(self)+"\n"
        for child in self.childrens:
            ret += child.__str__(level+1)
        return ret

    def __repr__(self):
        if self.isLeaf:
            return '{}'.format(self.label)
        else:
            return 'x{} > {}'.format(self.label, self.threshold)


if __name__ == '__main__':
    X = np.arange(10)[:, np.newaxis]
    y = (np.arange(10) > 5).astype(int)

    print('X = 5')
    t = DecisionTreeClassifier()
    t.fit(X, y)
    print(t)

    X = np.array([
        [0, 0, 1, 1],
        [0, 1, 0, 1]
    ]).T
    y = np.array([0, 1, 1, 0])

    print('XOR')
    t = DecisionTreeClassifier()
    t.fit(X, y)
    print(t)


    from sklearn.datasets import make_classification
    from sklearn.model_selection import train_test_split
    from sklearn.metrics import accuracy_score

    X, y = make_classification(n_samples=1000, n_features=4, n_informative=2, n_classes=2, random_state=0, shuffle=False)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

    t = DecisionTreeClassifier()
    t.fit(X_train, y_train)

    y_pred = t.predict(X_test)
    assert accuracy_score(y_test, y_pred) > 0.5
